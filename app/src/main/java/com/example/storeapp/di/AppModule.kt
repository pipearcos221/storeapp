package com.example.storeapp.di

import com.example.data.di.RepositoriesModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(
    includes = [RepositoriesModule::class]
)
@InstallIn(SingletonComponent::class)
object AppModule