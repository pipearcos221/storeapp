package com.example.data.repositories

import com.example.data.datasources.ProductRemoteDataSource
import com.example.domain.common.AsyncResponse
import com.example.domain.common.CustomError
import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail
import com.example.domain.repositories.ProductRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepositoryImpl @Inject constructor(
    private val productRemoteDataSource: ProductRemoteDataSource
) : ProductRepository {

    private val GENERIC_ERROR_MESSAGE = "Ha ocurrido un error en tu solicitud, por favor intentalo de nuevo mas tarde."

    override suspend fun getProductsListByQuery(query: String): AsyncResponse<List<Product>> = try {
        val products = productRemoteDataSource.productsListByQuery(query)
        AsyncResponse.success(products)
    } catch (exception: Exception) {
        AsyncResponse.error(CustomError(exception.cause, GENERIC_ERROR_MESSAGE))
    }

    override suspend fun getProductDetailById(id: String): AsyncResponse<ProductDetail> = try {
        val productDetail = productRemoteDataSource.productDetailById(id)
        AsyncResponse.success(productDetail)
    } catch (exception: Exception) {
        AsyncResponse.error(CustomError(exception.cause, GENERIC_ERROR_MESSAGE))
    }
}