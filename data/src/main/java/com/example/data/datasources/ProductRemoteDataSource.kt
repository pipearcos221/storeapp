package com.example.data.datasources

import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail

interface ProductRemoteDataSource {
    suspend fun productsListByQuery(query: String): List<Product>

    suspend fun productDetailById(id: String): ProductDetail
}

