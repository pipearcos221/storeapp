package com.example.data.di

import com.example.data.repositories.ProductRepositoryImpl
import com.example.domain.repositories.ProductRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoriesModule {

    @Binds
    @Singleton
    abstract fun bindProductRepository(productRepositoryImpl: ProductRepositoryImpl): ProductRepository

}
