package com.example.data.repositories

import com.example.data.datasources.ProductRemoteDataSource
import com.example.data.repositories.mocks.ProductMock
import com.example.domain.common.AsyncResponse
import com.example.domain.common.CustomError
import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductRepositoryImplTest {

    @Mock
    lateinit var productRemoteDataSource: ProductRemoteDataSource
    lateinit var productRepositoryImpl: ProductRepositoryImpl

    private val productlistMock: List<Product> = ProductMock.getProductList()
    private val productDetailMock: ProductDetail = ProductMock.getProductDetail()

    @Before
    fun setup() {
        productRepositoryImpl = ProductRepositoryImpl(productRemoteDataSource)
    }

    @Test
    fun `check get a list of products by query`() = runBlocking {
        //given
        val query = "query"
        val expectedValue = AsyncResponse.success(productlistMock)
        whenever(productRemoteDataSource.productsListByQuery(query)).thenReturn(productlistMock)

        //when
        val result = productRepositoryImpl.getProductsListByQuery(query)

        //then
        assertEquals(expectedValue, result)
    }

    @Test
    fun `check productList return an error when the request fail`() = runBlocking {
        //given
        val query = "query"
        val expectedValue = AsyncResponse.error<List<Product>>(
            CustomError(
                IllegalStateException().cause,
                "Ha ocurrido un error en tu solicitud, por favor intentalo de nuevo mas tarde."
            )
        )
        whenever(productRemoteDataSource.productsListByQuery(query)).thenThrow(IllegalStateException())

        //when
        val result = productRepositoryImpl.getProductsListByQuery(query)

        //then
        assertEquals(expectedValue, result)
    }

    @Test
    fun `check get a product detail by id`() = runBlocking {
        //given
        val id = "MCO615941202"
        val expectedValue = AsyncResponse.success(productDetailMock)
        whenever(productRemoteDataSource.productDetailById(id)).thenReturn(productDetailMock)

        //when
        val result = productRepositoryImpl.getProductDetailById(id)

        //then
        assertEquals(expectedValue, result)
    }

    @Test
    fun `check productDetail return an error when the request fail`() = runBlocking {
        //given
        val id = "MCO615941202"
        val expectedValue = AsyncResponse.error<ProductDetail>(
            CustomError(
                IllegalStateException().cause,
                "Ha ocurrido un error en tu solicitud, por favor intentalo de nuevo mas tarde."
            )
        )
        whenever(productRemoteDataSource.productDetailById(id)).thenThrow(IllegalStateException())

        //when
        val result = productRepositoryImpl.getProductDetailById(id)

        //then
        assertEquals(expectedValue, result)
    }
}