package com.example.data.repositories.mocks

import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail
import com.example.domain.utils.UJson

object ProductMock {

    fun getProductList(): List<Product> = UJson.getObjectFromJson("products.json", Array<Product>::class.java).toList()

    fun getProductDetail(): ProductDetail = UJson.getObjectFromJson("productDetail.json", ProductDetail::class.java)

}