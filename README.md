# Android Challenge - Mercado Libre

### Edgar Felipe Arcos Trejo
pipearcos221@gmail.com

(+57) 3013977229

&nbsp;

## Description
This is an application where you can search for products of your interest by keyword and view a list of results as well as the specific detail of a product

&nbsp;

## Preview

![Preview](preview.gif)

&nbsp;

## Architecture

For the development of the application, Clean Architecture was used, multi-module, in order to have an application decoupled, tolerant to changes and scalable over time, in turn, the application was divided into three layers, data, domain and presentation.

### **Data**
The data layer implements the repositories defined in the domain layer and it define the behavior of the data sources

### **Domain**
The domain is the heart of an application and it has to be totally isolated from any dependency unrelated to business logic or data.

- **Entities**:
 The Models can implement structures and business rules, in this case the information of the product and detail

- **Use Case**:
The use cases allow interaction with the architecture and implement the business logic

- **Repository**:
The repositories abstract the sources of information, in the domain layer only the behavior is defined but its implementation is unknown, this so that the data layer depends on the domain layer

### **Presentation**
The presentation layer includes the Activities or Fragments views, and state management through ViewModel and LiveData

![Architecture](architecture.png)


&nbsp;

## Good Practices

across the development of the project, the following principles and good practices were taken into account

 - CLean Architecture
 - SOLID Patterns
 - TDD-BDD
 - Continuous Integration
 - TellDontAsk
 - DRY (Don't repeat yourself)
 - Clean Code

&nbsp;

## Code Quality

### **Testing**

For the implementation of the unit tests, TDD was used, which is a practice where we seek to implement the unit tests first (Test First Development) and refactoring, as well as BDD (Behavior Driven Development) based on Give-When-Then was taken into account.

### **Crashlytics**
Firebase Crashlytics was configured in the project,  in order to register error logs in the application and be able to see them in the firebase console

### **Analytics**
Firebase Analytics was configured in the project, in order to record logs and be able to view them as events in the firebase console

### **Continuous Integration**
Added gitlab-ci.yml to run scripts to compile and test for every push made

&nbsp;

## Libraries
Official android libraries were used or that are widely used by the development community.

 * [Kotlin](http://kotlinlang.org/) as main language
 * [Kotlin Coroutines](https://developer.android.com/kotlin/coroutines?gclid=Cj0KCQiAifz-BRDjARIsAEElyGK5CyLrvgvGliSMbAreCzH3bW8tTX26pGhmyR4jkpaJBD5ZjcBQ_4waApIdEALw_wcB&gclsrc=aw.ds) for execute asynchronous tasks
 * [Android ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) for the MVVM pattern
 * [Android Navigation component](https://developer.android.com/guide/navigation/navigation-getting-started) for the app navigation
 * [Retrofit](https://square.github.io/retrofit/) for networking
 * [Coil](https://github.com/coil-kt/coil) for image loading
 * [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) for dependency injection
 * [mockito](https://site.mockito.org/) for testing

