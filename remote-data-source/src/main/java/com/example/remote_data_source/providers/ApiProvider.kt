package com.example.remote_data_source.providers

import com.example.remote_data_source.api.EndPoints
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiProvider @Inject constructor() {
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(EndPoints.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun <T> create(clazz: Class<T>): T = retrofit.create(clazz)

}
