package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class SearchPagingDTO(
    @SerializedName("total") val total: Int,
    @SerializedName("primary_results") val primaryResults: Int,
    @SerializedName("offset") val offset: Int,
    @SerializedName("limit") val limit: Int
)
