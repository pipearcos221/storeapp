package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class ImageDTO(
    @SerializedName("secure_url") val url: String
)
