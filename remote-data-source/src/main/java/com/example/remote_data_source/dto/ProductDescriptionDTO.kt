package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class ProductDescriptionDTO(
    @SerializedName("plain_text") val plainText: String
)
