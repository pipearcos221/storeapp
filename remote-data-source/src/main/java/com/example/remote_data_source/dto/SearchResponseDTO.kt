package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class SearchResponseDTO(
    @SerializedName("paging") val paging: SearchPagingDTO,
    @SerializedName("results") val results: List<ProductDTO>
)

