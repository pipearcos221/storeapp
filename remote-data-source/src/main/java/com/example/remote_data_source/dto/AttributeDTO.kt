package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class AttributeDTO(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("value_name") val value: String? = null
)