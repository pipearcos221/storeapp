package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class NameDTO(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String
)
