package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class AddressDTO(
    @SerializedName("city") val city: NameDTO,
    @SerializedName("state") val state: NameDTO
)

