package com.example.remote_data_source.dto

import com.example.domain.utils.UMoney
import com.google.gson.annotations.SerializedName

data class ProductDTO(
    @SerializedName("id") val id: String,
    @SerializedName("title")  val title: String,
    @SerializedName("price") val price: Double,
    @SerializedName("installments") val installments: InstallmentsDTO?,
    @SerializedName("thumbnail") val thumbnail: String
) {
    fun getInstallmentsAsString(): String = installments?.let { "en ${it.quantity}x ${UMoney.formatMoney(it.amount)} ${it.currencyId}" } ?: ""
}
