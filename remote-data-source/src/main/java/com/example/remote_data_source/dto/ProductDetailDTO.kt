package com.example.remote_data_source.dto

import com.example.domain.entities.ProductDetail
import com.google.gson.annotations.SerializedName

data class ProductDetailDTO(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("price") val price: Double,
    @SerializedName("pictures") val images: List<ImageDTO>,
    @SerializedName("condition") val condition: String,
    @SerializedName("sold_quantity") val soldQuantity: Int,
    @SerializedName("available_quantity") val availableQuantity: Int,
    @SerializedName("seller_address") val address: AddressDTO,
    @SerializedName("attributes") val attributes: List<AttributeDTO>,
    @SerializedName("warranty") val warranty: String? = null
) {
    fun getListImages(): List<String> = this.images.map { it.url }
    fun getAttributesAsProductAttribute(): List<ProductDetail.ProductAttribute> = this.attributes.map {
        ProductDetail.ProductAttribute(it.name, it.value ?: "No especifica")
    }
    fun getAddressAsString() = "${this.address.city.name}, ${this.address.state.name}"
}
