package com.example.remote_data_source.dto

import com.google.gson.annotations.SerializedName

data class InstallmentsDTO(
    @SerializedName("quantity") val quantity: Int,
    @SerializedName("amount") val amount: Double,
    @SerializedName("currency_id") val currencyId: String
)
