package com.example.remote_data_source.datasource

import com.example.data.datasources.ProductRemoteDataSource
import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail
import com.example.remote_data_source.api.ProductApi
import com.example.remote_data_source.mappers.ProductDetailMapper
import com.example.remote_data_source.mappers.ProductMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRemoteDataSourceImpl @Inject constructor(
    private val productApi: ProductApi,
    private val productMapper: ProductMapper,
    private val productDetailMapper: ProductDetailMapper
) : ProductRemoteDataSource {
    override suspend fun productsListByQuery(query: String): List<Product> {
        val products = productApi.searchProductosByQuery(query = query)
        return products.results.map { productMapper.DTOtoEntity(it) }
    }

    override suspend fun productDetailById(id: String): ProductDetail {
        val productDetail = productApi.productDetailById(id)
        val productDescription = productApi.productDescriptionById(id)
        return productDetailMapper.DTOtoEntity(productDetail, productDescription)
    }
}