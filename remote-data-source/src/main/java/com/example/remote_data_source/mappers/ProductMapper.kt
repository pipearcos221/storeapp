package com.example.remote_data_source.mappers

import com.example.domain.entities.Product
import com.example.remote_data_source.dto.ProductDTO
import javax.inject.Inject

class ProductMapper @Inject constructor() {

    fun DTOtoEntity(productDTO: ProductDTO) = Product(
        id = productDTO.id,
        title = productDTO.title,
        price = productDTO.price,
        installments = productDTO.getInstallmentsAsString(),
        image = productDTO.thumbnail
    )

}