package com.example.remote_data_source.mappers

import com.example.domain.entities.ProductDetail
import com.example.remote_data_source.dto.ProductDescriptionDTO
import com.example.remote_data_source.dto.ProductDetailDTO
import javax.inject.Inject

class ProductDetailMapper @Inject constructor() {
    fun DTOtoEntity(detail: ProductDetailDTO, description: List<ProductDescriptionDTO>) = ProductDetail(
        id = detail.id,
        title = detail.title,
        price = detail.price,
        images = detail.getListImages(),
        condition = detail.condition,
        soldQuantity = detail.soldQuantity,
        availableQuantity = detail.availableQuantity,
        description = if (description.isNotEmpty()) description.first().plainText else "",
        address = detail.getAddressAsString(),
        warranty = if (detail.warranty.isNullOrEmpty()) "Garantía no especificada" else detail.warranty,
        attributes = detail.getAttributesAsProductAttribute()
    )
}
