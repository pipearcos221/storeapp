package com.example.remote_data_source.di

import com.example.remote_data_source.api.ProductApi
import com.example.remote_data_source.providers.ApiProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun provideProductApi(apiProvider: ApiProvider): ProductApi = apiProvider.create(ProductApi::class.java)

}
