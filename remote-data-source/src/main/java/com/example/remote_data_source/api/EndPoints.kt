package com.example.remote_data_source.api

object EndPoints {
    const val SITE_ID = "site-id"
    const val SITE_ID_COLOMBIA = "MCO"
    const val PRODUCT_ID = "product-id"
    const val QUERY_PARAMETER = "q"

    const val URL_BASE = "https://api.mercadolibre.com/"
    const val SEARCH_PRODUCTS_BY_QUERY = "sites/{$SITE_ID}/search"
    const val PRODUCT_DETAIL = "items/{$PRODUCT_ID}"
    const val PRODUCT_DESCRIPTION = "items/{$PRODUCT_ID}/descriptions"
}