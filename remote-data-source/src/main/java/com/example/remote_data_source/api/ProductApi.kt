package com.example.remote_data_source.api

import com.example.remote_data_source.api.EndPoints.PRODUCT_DESCRIPTION
import com.example.remote_data_source.api.EndPoints.PRODUCT_DETAIL
import com.example.remote_data_source.api.EndPoints.PRODUCT_ID
import com.example.remote_data_source.api.EndPoints.QUERY_PARAMETER
import com.example.remote_data_source.api.EndPoints.SEARCH_PRODUCTS_BY_QUERY
import com.example.remote_data_source.api.EndPoints.SITE_ID
import com.example.remote_data_source.api.EndPoints.SITE_ID_COLOMBIA
import com.example.remote_data_source.dto.ProductDescriptionDTO
import com.example.remote_data_source.dto.ProductDetailDTO
import com.example.remote_data_source.dto.SearchResponseDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductApi {

    @GET(SEARCH_PRODUCTS_BY_QUERY)
    suspend fun searchProductosByQuery(
        @Path(SITE_ID) site: String = SITE_ID_COLOMBIA,
        @Query(QUERY_PARAMETER) query: String
    ): SearchResponseDTO

    @GET(PRODUCT_DETAIL)
    suspend fun productDetailById(@Path(PRODUCT_ID) productId: String): ProductDetailDTO

    @GET(PRODUCT_DESCRIPTION)
    suspend fun productDescriptionById(@Path(PRODUCT_ID) productId: String): List<ProductDescriptionDTO>
}
