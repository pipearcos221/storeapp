package com.example.domain.mocks

import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail
import com.example.domain.utils.UJson
import com.google.gson.Gson
import java.io.File
import javax.rmi.CORBA.PortableRemoteObjectDelegate
import kotlin.reflect.KClass

object ProductMock {

    fun getProductList(): List<Product> = UJson.getObjectFromJson("products.json", Array<Product>::class.java).toList()

    fun getProductDetail(): ProductDetail = UJson.getObjectFromJson("productDetail.json", ProductDetail::class.java)

}