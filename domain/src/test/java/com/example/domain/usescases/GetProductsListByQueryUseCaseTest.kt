package com.example.domain.usescases

import com.example.domain.common.AsyncResponse
import com.example.domain.common.CustomError
import com.example.domain.common.StatusTask
import com.example.domain.entities.Product
import com.example.domain.mocks.ProductMock
import com.example.domain.repositories.ProductRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetProductsListByQueryUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    lateinit var getProductsListByQueryUseCase: GetProductsListByQueryUseCase

    private val productListMock: List<Product> = ProductMock.getProductList()

    @Before
    fun setup() {
        getProductsListByQueryUseCase = GetProductsListByQueryUseCase(productRepository)
    }

    @Test
    fun `check get list of products by query`() = runBlocking {
        //given
        val query = "query"
        val expectedValue = AsyncResponse.success(productListMock)
        whenever(productRepository.getProductsListByQuery(query)).thenReturn(expectedValue)

        //when
        val result = getProductsListByQueryUseCase(query)

        //then
        assertEquals(expectedValue, result)
    }

    @Test
    fun `check return a AsyncReponse with status error when the request fail`() = runBlocking {
        //given
        val query = "query"
        val expectedValue = AsyncResponse.error<List<Product>>(CustomError(customMessage = "Ha ocurrido un error en tu solicitud, por favor intentalo de nuevo mas tarde."))
        whenever(productRepository.getProductsListByQuery(query)).thenReturn(expectedValue)

        //when
        val result = getProductsListByQueryUseCase(query)

        //then
        assertEquals(StatusTask.FAILURE, result.status)
    }

}