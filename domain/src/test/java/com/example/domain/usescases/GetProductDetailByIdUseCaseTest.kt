package com.example.domain.usescases

import com.example.domain.common.AsyncResponse
import com.example.domain.common.CustomError
import com.example.domain.common.StatusTask
import com.example.domain.entities.ProductDetail
import com.example.domain.mocks.ProductMock
import com.example.domain.repositories.ProductRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetProductDetailByIdUseCaseTest {

    @Mock
    lateinit var productRepository: ProductRepository
    lateinit var getProductDetailByIdUseCase: GetProductDetailByIdUseCase

    private val productDetailMock: ProductDetail = ProductMock.getProductDetail()

    @Before
    fun setup() {
        getProductDetailByIdUseCase = GetProductDetailByIdUseCase(productRepository)
    }

    @Test
    fun `check get a product detail by id`() = runBlocking {
        //given
        val id = "MCO615941202"
        val expectedValue = AsyncResponse.success(productDetailMock)
        whenever(productRepository.getProductDetailById(id)).thenReturn(expectedValue)

        //when
        val result = getProductDetailByIdUseCase(id)

        //then
        assertEquals(expectedValue, result)
    }

    @Test
    fun `check return a AsyncResponse with status failure when the request fail`() = runBlocking {
        //given
        val id = "MCO615941202"
        val expectedValue = AsyncResponse.error<ProductDetail>(CustomError(customMessage = "Ha ocurrido un error en tu solicitud, por favor intentalo de nuevo mas tarde."))
        whenever(productRepository.getProductDetailById(id)).thenReturn(expectedValue)

        //when
        val result = getProductDetailByIdUseCase(id)

        //then
        assertEquals(StatusTask.FAILURE, result.status)
    }
}