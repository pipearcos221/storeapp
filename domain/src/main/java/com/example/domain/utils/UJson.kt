package com.example.domain.utils

import com.google.gson.Gson
import java.io.File

object UJson {
    fun getJson(path: String): String =
        this.javaClass.classLoader.getResource(path)?.let {
            val file = File(it.path)
            String(file.readBytes())
        }.orEmpty()

    fun <T: Any> getObjectFromJson(pathFile: String, clazz: Class<T>): T = Gson().fromJson(getJson(pathFile), clazz)
}