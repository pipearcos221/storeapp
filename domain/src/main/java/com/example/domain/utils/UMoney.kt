package com.example.domain.utils

import java.text.NumberFormat
import java.util.*

class UMoney {

    companion object {
        fun formatMoney(value: Number): String {
            val locale = Locale("es", "CO")
            return NumberFormat.getCurrencyInstance(locale).apply {
                currency = Currency.getInstance(locale)
                maximumFractionDigits = 2
                minimumFractionDigits = 0
            }.format(value)
        }
    }

}