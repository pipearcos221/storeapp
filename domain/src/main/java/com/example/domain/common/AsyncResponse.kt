package com.example.domain.common

data class AsyncResponse<out T>(
    val status: StatusTask,
    val data: T? = null,
    val error: CustomError? = null
) {
    companion object {
        fun <T> loading(): AsyncResponse<T> = AsyncResponse(status = StatusTask.LOADING)
        fun <T> success(data: T): AsyncResponse<T> = AsyncResponse(status = StatusTask.SUCCESS, data = data)
        fun <T> error(error: CustomError): AsyncResponse<T> = AsyncResponse(status = StatusTask.FAILURE, error = error)
    }

}
