package com.example.domain.common

enum class StatusTask {
    LOADING,
    SUCCESS,
    FAILURE
}