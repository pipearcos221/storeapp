package com.example.domain.common

data class CustomError(
    val throwable: Throwable? = null,
    val customMessage: String
)
