package com.example.domain.repositories

import com.example.domain.common.AsyncResponse
import com.example.domain.entities.Product
import com.example.domain.entities.ProductDetail

interface ProductRepository {

    suspend fun getProductsListByQuery(query: String): AsyncResponse<List<Product>>

    suspend fun getProductDetailById(id: String): AsyncResponse<ProductDetail>

}