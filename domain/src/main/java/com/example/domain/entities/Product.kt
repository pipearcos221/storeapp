package com.example.domain.entities

data class Product(
        val id: String,
        val title: String,
        val price: Double,
        val installments: String,
        val image: String
)