package com.example.domain.entities

data class ProductDetail(
        val id: String,
        val title: String,
        val price: Double,
        val images: List<String>,
        val condition: String,
        val soldQuantity: Int,
        val availableQuantity: Int,
        val description: String,
        val address: String,
        val warranty: String,
        val attributes: List<ProductAttribute>
) {
        data class ProductAttribute(val name: String, val value: String)
}
