package com.example.domain.usescases

import com.example.domain.common.AsyncResponse
import com.example.domain.common.StatusTask
import com.example.domain.entities.Product
import com.example.domain.repositories.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetProductsListByQueryUseCase @Inject constructor(private val productRepository: ProductRepository){

    suspend operator fun invoke(query: String): AsyncResponse<List<Product>> = withContext(Dispatchers.IO) {
        productRepository.getProductsListByQuery(query)
    }

}