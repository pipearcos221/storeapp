package com.example.domain.usescases

import com.example.domain.common.AsyncResponse
import com.example.domain.entities.ProductDetail
import com.example.domain.repositories.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetProductDetailByIdUseCase @Inject constructor(private val productRepository: ProductRepository) {

    suspend operator fun invoke(id:String): AsyncResponse<ProductDetail> = withContext(Dispatchers.IO) {
        productRepository.getProductDetailById(id)
    }

}