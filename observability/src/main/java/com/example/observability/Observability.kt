package com.example.observability

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class Observability(context: Context?) {

    private val firebaseRegister = FirebaseAnalytics.getInstance(context)

    fun registrarEvento(event: String, parameters: MutableMap<String, String>) {
        val args = Bundle().apply {
            parameters.map { putString(it.key, it.value) }
        }
        firebaseRegister.logEvent(event, args)
    }
}