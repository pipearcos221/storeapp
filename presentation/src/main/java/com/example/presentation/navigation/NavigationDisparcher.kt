package com.example.presentation.navigation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import com.example.presentation.common.Event
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

typealias  NavigationCommand  = (NavController) ->  Unit

@ActivityRetainedScoped
class NavigationDispatcher @Inject constructor() {

    private val navigationEmitter: MutableLiveData<Event<NavigationCommand>> = MutableLiveData()
    val navigationCommands: LiveData<Event<NavigationCommand>> = navigationEmitter

    fun emit(navigationCommand: NavigationCommand) {
        navigationEmitter.value = Event(navigationCommand)
    }

}