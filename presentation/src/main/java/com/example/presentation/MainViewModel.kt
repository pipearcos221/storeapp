package com.example.presentation

import androidx.lifecycle.ViewModel
import com.example.presentation.navigation.NavigationDispatcher
import com.example.presentation.productList.ProductListFragmentDirections
import com.example.presentation.search.SearchFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val navigationDispatcher: NavigationDispatcher
) : ViewModel() {

    fun goToProductList(query: String) {
        navigationDispatcher.emit { navController ->
            when (navController.currentDestination?.id) {
                R.id.searchFragment -> {
                    SearchFragmentDirections.actionSearchToProductList(query)
                }
                R.id.productListFragment -> {
                    ProductListFragmentDirections.actionProductListSelf(query)
                }
                else -> null
            }?.let { navController.navigate(it) }
        }
    }
}