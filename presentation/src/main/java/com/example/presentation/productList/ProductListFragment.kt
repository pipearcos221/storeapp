package com.example.presentation.productList

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.domain.common.AsyncResponse
import com.example.domain.common.StatusTask
import com.example.domain.entities.Product
import com.example.observability.CATEGORIAS_ANALITICA
import com.example.observability.Observability
import com.example.presentation.R
import com.example.presentation.adapters.productList.ProductListAdapter
import com.example.presentation.databinding.FragmentProductListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : Fragment() {

    lateinit var binding: FragmentProductListBinding
    private val viewModel: ProductListViewModel by viewModels()
    private val args: ProductListFragmentArgs by navArgs()
    private val adapter by lazy { ProductListAdapter(this::goToProductDetail) }
    private val view = "ProductListFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registerAnalitics()
        setupSearchView()
        setupRecyclerView()
        setupListener()
        setupData()
    }

    private fun registerAnalitics() = Observability(activity?.applicationContext).registrarEvento(
        event = CATEGORIAS_ANALITICA.PANTALLA.name,
        parameters = mutableMapOf("view" to view)
    )

    private fun setupListener() {
        binding.viewError.buttonRetry.setOnClickListener { setupData() }
    }

    private fun setupSearchView() {
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        binding.searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            isQueryRefinementEnabled = true
            queryHint = context.getString(R.string.text_search_a_product)
            setQuery(args.query, false)
        }
    }

    private fun setupRecyclerView() {
        binding.productList.adapter = adapter
    }

    private fun setupData() {
        viewModel.searchProductByQuery(args.query)
        viewModel.productListState.observe(viewLifecycleOwner) {
            handleState(it.getContentIfNotHandled())
        }
    }

    private fun handleState(content: AsyncResponse<List<Product>>?) {
        hideLoader()
        hideEmptyDataScreen()
        hideErrorScreen()
        when (content?.status) {
            StatusTask.LOADING -> showLoader()
            StatusTask.SUCCESS -> handleData(content.data)
            StatusTask.FAILURE -> showErrorScreen()
        }
    }

    private fun handleData(data: List<Product>?) = data?.let {
        if (it.isEmpty()) showEmptyDataScreen() else adapter.productList = it
    } ?: showErrorScreen()

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showEmptyDataScreen() {
        binding.viewEmpty.root.visibility = View.VISIBLE
    }

    private fun hideEmptyDataScreen() {
        binding.viewEmpty.root.visibility = View.GONE
    }

    private fun showErrorScreen() {
        binding.viewError.root.visibility = View.VISIBLE
    }

    private fun hideErrorScreen() {
        binding.viewError.root.visibility = View.GONE
    }

    private fun goToProductDetail(idProduct: String) = viewModel.goToDetail(idProduct)

}