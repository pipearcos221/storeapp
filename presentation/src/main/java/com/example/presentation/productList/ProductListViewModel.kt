package com.example.presentation.productList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.common.AsyncResponse
import com.example.domain.entities.Product
import com.example.domain.usescases.GetProductsListByQueryUseCase
import com.example.presentation.common.Event
import com.example.presentation.navigation.NavigationDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    private val navigationDispatcher: NavigationDispatcher,
    private val getProductsListByQueryUseCase: GetProductsListByQueryUseCase
) : ViewModel() {

    private val _productListState: MutableLiveData<Event<AsyncResponse<List<Product>>>> =
        MutableLiveData()
    val productListState: LiveData<Event<AsyncResponse<List<Product>>>> = _productListState

    fun searchProductByQuery(query: String) = viewModelScope.launch(Dispatchers.Main) {
        _productListState.value = Event(AsyncResponse.loading())
        _productListState.value = Event(getProductsListByQueryUseCase(query))
    }

    fun goToDetail(idProduct: String) {
        navigationDispatcher.emit { navController ->
            ProductListFragmentDirections.actionProductListToProductDetail(idProduct)
                .let { navController.navigate(it) }
        }
    }

}