package com.example.presentation.dialogs.fullscreencarouselimage

import androidx.lifecycle.ViewModel
import com.example.presentation.navigation.NavigationDispatcher
import com.example.presentation.productDetail.ProductDetailFragment.Companion.PAGE
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FullScreenCarouselImageViewModel @Inject constructor(
    private val navigationDispatcher: NavigationDispatcher
) : ViewModel() {

    fun setCurrentPage(page: Int) = navigationDispatcher.emit {
        it.previousBackStackEntry?.savedStateHandle?.set(PAGE, page)
    }

}