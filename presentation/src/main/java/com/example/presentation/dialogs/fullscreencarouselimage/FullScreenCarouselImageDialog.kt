package com.example.presentation.dialogs.fullscreencarouselimage

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.presentation.databinding.DialogFullscreenCarouselImageBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FullScreenCarouselImageDialog : DialogFragment() {

    private lateinit var binding: DialogFullscreenCarouselImageBinding
    private val viewMode: FullScreenCarouselImageViewModel by viewModels()
    private val args: FullScreenCarouselImageDialogArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogFullscreenCarouselImageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.carouselImageView.setImages(args.images.toList(), args.currentPage) {}
        binding.buttonClose.setOnClickListener {
            viewMode.setCurrentPage(binding.carouselImageView.currentPage)
            dismiss()
        }
    }

}