package com.example.presentation.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager2.widget.ViewPager2
import com.example.presentation.R
import com.example.presentation.adapters.components.CarouselImageAdapter
import com.example.presentation.databinding.ViewCarouselImageBinding

class CarouselImageView(context: Context, attrs: AttributeSet? = null) :
    ConstraintLayout(context, attrs) {

    private val adapter by lazy { CarouselImageAdapter() }
    private val binding = ViewCarouselImageBinding.inflate(LayoutInflater.from(context), this)
    val currentPage: Int
        get() = binding.imagePager.currentItem

    init {
        binding.imagePager.adapter = adapter
    }

    fun setImages(images: List<String>, currentPage: Int = 0, listener: (() -> Unit)) {
        adapter.apply {
            this.images = images
            this.listener = listener
        }
        binding.apply {
            imageCounter.text = resources.getString(R.string.text_image_counter, currentPage.inc(), images.size)
            imagePager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    imageCounter.text = resources.getString(R.string.text_image_counter, position.inc(), images.size)
                }
            })
        }
        setCurrentPage(currentPage)
     }

    fun setCurrentPage(page: Int) {
        binding.imagePager.setCurrentItem(page, false)
    }

}