package com.example.presentation.productDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.common.AsyncResponse
import com.example.domain.entities.ProductDetail
import com.example.domain.usescases.GetProductDetailByIdUseCase
import com.example.presentation.common.Event
import com.example.presentation.navigation.NavigationDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    private val navigationDispatcher: NavigationDispatcher,
    private val getProductDetailByIdUseCase: GetProductDetailByIdUseCase
) : ViewModel() {

    private val _productDetailState: MutableLiveData<Event<AsyncResponse<ProductDetail>>> = MutableLiveData()
    val productDetailState: LiveData<Event<AsyncResponse<ProductDetail>>> = _productDetailState

    fun getProductDetailById(idProduct: String) = viewModelScope.launch(Dispatchers.Main) {
        _productDetailState.value = Event(AsyncResponse.loading())
        _productDetailState.value = Event(getProductDetailByIdUseCase(idProduct))
    }

    fun goToFullScreenCarouselImage(images: List<String>, currentPage: Int = 0) {
        navigationDispatcher.emit { navController ->
            ProductDetailFragmentDirections.actionProductDetailToFullscreen(
                images.toTypedArray(),
                currentPage
            ).let { navController.navigate(it) }
        }
    }


}