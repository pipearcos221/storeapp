package com.example.presentation.productDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.domain.common.AsyncResponse
import com.example.domain.common.StatusTask
import com.example.domain.entities.ProductDetail
import com.example.domain.utils.UMoney
import com.example.observability.CATEGORIAS_ANALITICA
import com.example.observability.Observability
import com.example.presentation.R
import com.example.presentation.adapters.productDetail.FeaturesProductAdapter
import com.example.presentation.databinding.FragmentProductDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    companion object {
        const val PAGE = "PAGE"
    }

    private lateinit var binding: FragmentProductDetailBinding
    private val args: ProductDetailFragmentArgs by navArgs()
    private val viewModel: ProductDetailViewModel by viewModels()
    private val adapter by lazy { FeaturesProductAdapter() }
    private val view = "ProductDetailFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registerAnalitics()
        setupToolbar()
        setupRecycler()
        setupData()
        setupListeners()
    }

    private fun registerAnalitics() = Observability(activity?.applicationContext).registrarEvento(
        event = CATEGORIAS_ANALITICA.PANTALLA.name,
        parameters = mutableMapOf("view" to view)
    )

    private fun setupToolbar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
    }

    private fun setupRecycler() {
        binding.containerDetail.featureList.adapter = adapter
    }

    private fun setupData() {
        viewModel.getProductDetailById(args.idProduct)
        viewModel.productDetailState.observe(viewLifecycleOwner) {
            handleState(it.getContentIfNotHandled())
        }
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Int>(PAGE)?.observe(viewLifecycleOwner){
            binding.containerDetail.carouselImageView.setCurrentPage(it)
        }
    }

    private fun handleState(content: AsyncResponse<ProductDetail>?) = content?.let {
        hideLoader()
        hideErrorScreen()
        when (content.status) {
            StatusTask.LOADING -> showLoader()
            StatusTask.SUCCESS -> handleData(content.data)
            StatusTask.FAILURE -> showErrorScreen()
        }
    }

    private fun handleData(productDetail: ProductDetail?) = productDetail?.let {
        binding.containerDetail.run {
            conditionAndSoldQuantity.text = resources.getString(
                R.string.text_condition_and_sold_quantity,
                it.condition,
                it.soldQuantity
            )
            title.text = it.title
            setImagesToCarouselView(it.images)
            available.text = getString(R.string.text_products_available, it.availableQuantity)
            price.text = UMoney.formatMoney(it.price)
            warranty.text = it.warranty
            address.text = it.address
            description.text = it.description
        }
        adapter.featureList = it.attributes
    }

    private fun setImagesToCarouselView(images: List<String>) = binding.containerDetail.run {
        carouselImageView.setImages(images) {
            viewModel.goToFullScreenCarouselImage(images, carouselImageView.currentPage)
        }
    }

    private fun setupListeners() {
        binding.containerErrorData.buttonRetry.setOnClickListener {
            viewModel.getProductDetailById(args.idProduct)
        }
    }

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showErrorScreen() {
        binding.containerErrorData.root.visibility = View.VISIBLE
    }

    private fun hideErrorScreen() {
        binding.containerErrorData.root.visibility = View.GONE
    }

}