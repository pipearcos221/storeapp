package com.example.presentation.adapters.productList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.domain.entities.Product
import com.example.domain.utils.UMoney
import com.example.presentation.R
import com.example.presentation.databinding.ItemProductBinding

class ProductListAdapter(val onClick: (String) -> Unit) :
    RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    var productList: List<Product> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder =
        ProductViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        )

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(productList[position], onClick)
    }

    override fun getItemCount(): Int = productList.size

    class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemProductBinding.bind(view)
        fun bind(product: Product, onClick: (String) -> Unit) = binding.apply {
            image.load(product.image)
            title.text = product.title
            price.text = UMoney.formatMoney(product.price)
            installments.text = product.installments
            root.setOnClickListener { onClick.invoke(product.id) }
        }

    }
}