package com.example.presentation.adapters.productDetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entities.ProductDetail
import com.example.presentation.R
import com.example.presentation.databinding.ItemFeaturesProductBinding

class FeaturesProductAdapter : RecyclerView.Adapter<FeaturesProductAdapter.FeatureViewHolder>() {

    var featureList: List<ProductDetail.ProductAttribute> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatureViewHolder =
        FeatureViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_features_product, parent, false))

    override fun onBindViewHolder(holder: FeatureViewHolder, position: Int) =
        holder.bind(featureList[position])

    override fun getItemCount(): Int = featureList.size

    class FeatureViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemFeaturesProductBinding.bind(view)
        fun bind(productAttribute: ProductDetail.ProductAttribute) {
            binding.nameFeature.text = productAttribute.name
            binding.valueFeature.text = productAttribute.value
        }
    }
}