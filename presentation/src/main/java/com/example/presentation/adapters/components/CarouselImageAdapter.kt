package com.example.presentation.adapters.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.presentation.R
import com.example.presentation.databinding.ItemCarouselImageBinding

class CarouselImageAdapter : RecyclerView.Adapter<CarouselImageAdapter.ImageViewHolder>() {

    var listener: (() -> Unit) = {}
    var images: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_carousel_image, parent, false)
        )

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) =
        holder.bind(images[position], listener)

    override fun getItemCount(): Int = images.size

    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemCarouselImageBinding.bind(view)
        fun bind(url: String, listener: (() -> Unit)) {
            binding.image.load(url)
            binding.root.setOnClickListener { listener.invoke() }
        }
    }
}