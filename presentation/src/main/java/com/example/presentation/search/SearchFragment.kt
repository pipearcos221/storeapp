package com.example.presentation.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.observability.CATEGORIAS_ANALITICA
import com.example.observability.Observability
import com.example.presentation.R
import com.example.presentation.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding
    private val view = "SearchFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registerAnalitics()
        setupSearchView()
    }

    private fun registerAnalitics() = Observability(activity?.applicationContext).registrarEvento(
        event = CATEGORIAS_ANALITICA.PANTALLA.name,
        parameters = mutableMapOf("view" to view)
    )

    private fun setupSearchView() {
        val searchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        binding.searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            isQueryRefinementEnabled = true
            queryHint = context.getString(R.string.text_search_a_product)
        }
    }
}